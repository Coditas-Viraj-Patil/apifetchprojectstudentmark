import { Component } from '@angular/core';
import { HttpserviceService } from './httpservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  datas: any;
  all: number = 0;
  partial: number = 0;
  none: number = 0;

  constructor(private httpservice: HttpserviceService) {}
  ngOnInit() {
    this.httpservice.getData().subscribe((task) => {
      console.log(task);
      this.datas = task;
      const { data } = this.datas;

      for (let dataofstudents of data) {
        let datasOfStudent = dataofstudents['marksInfo'];
        let count = 0;
        let length = 0;

        for (let markInfo of datasOfStudent) {
          if (markInfo.obtainedMarks == 1) {
            count += 1;
          }
          length += 1;
        }
        if (count == length) {
          this.all += 1;
        } else if (count != length && count != 0) {
          this.partial += 1;
        } else {
          this.none += 1;
        }
      }
      console.log(this.all);
      console.log(this.partial);
      console.log(this.none);
    });
  }
}
