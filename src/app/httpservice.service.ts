import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class HttpserviceService {
  constructor(private http: HttpClient) {}
  getData() {
    let url = 'https://saral-dev-api.anuvaad.org/getMarksReport';
    return this.http.get(url);
  }
}
